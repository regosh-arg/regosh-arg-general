# reGOSH Arg - Coordinación General

Proyecto de coordinación general de los nodos reGOSH en Argentina.

La lista oficial de integrantes de todos los nodos de reGOSH se encuentra en el sitio de reGOSH:

 * https://regosh.libres.cc/sobre-regosh/nodos-de-la-red/

Para contactarlos a los nodos de Mendoza o Buenos Aires se puede usar cualquier canal activo: principalmente a tráves del [foro de GOSH](https://forum.openhardware.science/c/latino-america-y-caribe/46), y como medio secundario los [repositorios de gitlab](https://gitlab.com/regosh-arg/proyectos) o a traves de la información en el [sitio de reGOSH](https://regosh.libres.cc/)), y/o directamente a su integrantes y coordinador@s.

El grupo de GitLab que contiene a este repositoro lista [algunos de los integrantes](https://gitlab.com/groups/regosh-arg/-/group_members) de reGOSH en Argentina.
